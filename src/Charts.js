import { useEffect, useState } from 'react';
import styles from './css/Charts.module.css';
import { BarChart } from './BarChart';
import { BeatLoader } from 'react-spinners';
import { MenuRow } from './MenuRow';

export const Charts = ({
    setSelectedPage, selectedPage, setIsLoginSuccess
}) => {

    const [avgEui, setAvgEui] = useState([]);
    const [showErrorMsg, setShowErrorMsg] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const getAvgEui = async () => {
        try{
            setIsLoading(true);
            const token = localStorage.getItem('test3App');

            const response = await fetch("/app/avg_eui", {
                method: "GET",
                headers: {"authorization": `Bearer ${token}`}
            });
            const responseData = await response.json();
    
            if(responseData.isGetDataSuccess){
                setIsLoading(false);
                setAvgEui(responseData.information)
                setShowErrorMsg(false);
            }else{
                if(responseData.isUnauthorized){
                    setIsLoginSuccess(false);
                    setAvgEui([]);
                }
                setAvgEui([]);
                setShowErrorMsg(true);
            }
        }catch(error){
            console.log(error)
        }
    }

    useEffect(() => {
        if(selectedPage === 2){
            getAvgEui();
        }
    }, [selectedPage]);

    const logout = () => {
        setIsLoginSuccess(false);
        localStorage.removeItem("test3App");
    }

    return (
        <div className={styles.chartsPage}>
            <MenuRow setSelectedPage={setSelectedPage} selectedPage={selectedPage}/>
            <div className={styles.infoContainer}>
                <div className={styles.eui}>
                    <p>Avg. EUI By Property Type</p>
                </div>
                {showErrorMsg&& <p className={styles.errorMsg}>*Failed to get data, please login again!</p>}
                <div className={styles.euiPicture}>
                    <div className={styles.loading}>
                        <BeatLoader size={30} color="grey" loading={isLoading} />
                    </div>
                    {!isLoading&& <BarChart avgEui={avgEui}/>}
                </div>
            </div>
            <div className={styles.buttonRow}>
                <button className={styles.signOutButton} onClick={logout}>SIGN OUT</button>
            </div>
        </div>
    )
}