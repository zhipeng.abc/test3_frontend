import { GoogleMap, Marker } from '@react-google-maps/api';
import styles from './css/Map.module.css';

export const Map = ({
    center
}) => {
    return <GoogleMap zoom={15} center={center} 
        mapContainerClassName={styles.mapContainer}>
            <Marker position={center}/>
    </GoogleMap>
}