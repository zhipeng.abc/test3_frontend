import { useState } from 'react';
import styles from './css/Login.module.css';

export const Login = ({
    setIsLoginSuccess
}) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [showErrorMsg, setShowErrorMsg] = useState(false);

    const login = async () => {
        try {
            
            const response = await fetch('/login', {
                method: "POST",
                headers: {"Content-type": "application/json"},
                body: JSON.stringify({
                    username,
                    password
                }),
            });
            const data = await response.json();

            if(data.isLoginSuccess){
                setIsLoginSuccess(true);
                setShowErrorMsg(false);
                localStorage.setItem('test3App', data.token)
            }else{
                setIsLoginSuccess(false);
                setShowErrorMsg(true);
            }
        }catch(error){
            console.log(error)
        }
    }
      

    return (
        <div className={styles.loginPage}>
            <div className={styles.loginContainer}>
                <p>SEATTLE BUILDING DATA VISUALIZATION</p>
                <div className={styles.usernameRow}>
                    <p>USERNAME</p>
                    <input type="text" onChange={(e) => setUsername(e.target.value)} />
                </div>
                <div className={styles.passwordRow}>
                    <p>PASSWORD</p>
                    <input type="password" onChange={(e) => setPassword(e.target.value)} />
                </div>
                { showErrorMsg&& <p className={styles.errorMsg}>*Please enter correct username or password!</p> }
                <div>
                    <button className={styles.loginBtn} onClick={login}>LOGIN</button>
                </div>
            </div>
        </div>
    )
}
