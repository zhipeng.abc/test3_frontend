import Chart from 'react-apexcharts';
import styles from './css/BarChart.module.css';

export const BarChart = ({
    avgEui
}) => {
    const options = {
        chart: {
            id: 'Avg. EUI By Property Type'
        },
        xaxis: {
            categories: avgEui.map((item) => item.type)
        },
        theme: {
            monochrome: {
              enabled: true,
              color: '#04AA6D',
              shadeTo: 'light',
              shadeIntensity: 0.65
            }
        }
    }

    const series = [{
        name: 'Avg.EUI',
        data: avgEui.map((item) => {
            const threeDecimals = Number(item.average_eui).toFixed(3);
            return threeDecimals;
        })
    }]


    return (
        <div className={styles.barChart}>
            <Chart
                type="bar" width={avgEui.length * 40} height={320}
                options={options} series={series}
            />
        </div>
    )
}