import './App.css';
import { Login } from './Login';
import { Overview } from './Overview';
import { Charts } from './Charts';
import { useState } from 'react';


function App() {

    const [isLoginSuccess, setIsLoginSuccess] = useState(false);
	const [selectedPage, setSelectedPage] = useState(1);

	return (
		<div className="App">
			{!isLoginSuccess&& <Login isLoginSuccess={isLoginSuccess} setIsLoginSuccess={setIsLoginSuccess}/>}
			{isLoginSuccess&& 
				(selectedPage === 1? 
					<Overview isLoginSuccess={isLoginSuccess} setSelectedPage={setSelectedPage} selectedPage={selectedPage} setIsLoginSuccess={setIsLoginSuccess}/> 
					: <Charts setSelectedPage={setSelectedPage} selectedPage={selectedPage} setIsLoginSuccess={setIsLoginSuccess}/>
				)
			}
			
		</div>
	);
}

export default App;
