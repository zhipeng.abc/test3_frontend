import { useEffect, useState } from 'react';
import { key } from './key';
import styles from './css/Overview.module.css';
import { Map } from './Map.js';
import { useLoadScript } from '@react-google-maps/api';
import { BeatLoader } from 'react-spinners';
import { MenuRow } from './MenuRow';

export const Overview = ({
    isLoginSuccess, setSelectedPage, selectedPage, setIsLoginSuccess
}) => {

    const [data, setData] = useState([]);
    const [showErrorMsg, setShowErrorMsg] = useState(false);
    const [displayFiveButtonIndex, setDisplayFiveButtonIndex] = useState(0);
    const [displayFiveButton, setDisplayFiveButton] = useState([]);
    const [disableNextBtn, setDisableNextBtn] = useState(false);
    const [disableBackBtn, setDisableBackBtn] = useState(false);
    const [selectedProperty, setSelectedProperty] = useState("");
    const [location, setLocation] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: key
    })

    const getData = async () => {
        try {
            setIsLoading(true);
            const token = localStorage.getItem('test3App');

            const response = await fetch("/app/buildings_info", {
                method: "GET",
                headers: { "authorization": `Bearer ${token}` },
            });
            const responseData = await response.json();

            if (responseData.isGetDataSuccess) {
                setIsLoading(false);
                setData([...responseData.buildingsInfo])
                setShowErrorMsg(false);
            } else {
                if (responseData.isUnauthorized) {
                    setIsLoginSuccess(false)
                    setData([]);
                }
                setData([])
                setShowErrorMsg(true);
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        if (isLoginSuccess) {
            getData();
        }
    }, [isLoginSuccess]);

    useEffect(() => {
        nextFiveBtn(0)
    }, [data]);


    useEffect(() => {
        if (displayFiveButtonIndex >= data.length - 1) {
            setDisableNextBtn(true);
        } else {
            setDisableNextBtn(false);
        }

        if (displayFiveButtonIndex - 5 <= 0) {
            setDisableBackBtn(true);
        } else {
            setDisableBackBtn(false);
        }
    }, [data, displayFiveButtonIndex]);

    useEffect(() => {
        if (selectedProperty)
            return setLocation({
                lat: Number(selectedProperty.Latitude),
                lng: Number(selectedProperty.Longitude)
            })
    }, [selectedProperty])

    const nextFiveBtn = (displayFiveButtonIndex) => {
        const nextFiveBtn = data.slice(displayFiveButtonIndex, displayFiveButtonIndex + 5);
        setDisplayFiveButtonIndex(displayFiveButtonIndex + 5);
        setDisplayFiveButton([...nextFiveBtn]);
    }

    const backFiveBtn = (displayFiveButtonIndex) => {
        const backFiveBtn = data.slice(displayFiveButtonIndex - 10, displayFiveButtonIndex - 5);
        setDisplayFiveButtonIndex(displayFiveButtonIndex - 5);
        setDisplayFiveButton(backFiveBtn);
    }

    const selectedPropertyItem = (selectedItemName) => {
        const selectedItem = data.filter((item) => {
            return item.PropertyName === selectedItemName
        })
        setSelectedProperty(selectedItem[0]);
    }

    const logout = () => {
        setIsLoginSuccess(false);
        localStorage.removeItem("test3App");
    }

    return (
        <div className={styles.overviewPage}>
            <MenuRow setSelectedPage={setSelectedPage} selectedPage={selectedPage}/>
            <div className={styles.infoContainer}>
                {showErrorMsg && <p className={styles.errorMsg}>*Failed to get data, please login again!</p>}
                <div className={styles.infoCard}>
                    <div className={styles.infoText}>
                        {selectedProperty &&
                            <>
                                <p>{selectedProperty.PropertyName}</p>
                                <p>{selectedProperty.PrimaryPropertyType}</p>
                                <p>{selectedProperty.Address}</p>
                                <p>{`# of floor: ${selectedProperty.NumberofFloors}`}</p>
                                <p>{`District: ${selectedProperty.CouncilDistrictCode}`}</p>
                                <p>{`Built in ${selectedProperty.YearBuilt}`}</p>
                            </>
                        }
                    </div>
                    <div className={styles.infoPicture}>
                        {(location && isLoaded) && <Map center={location} />}
                    </div>
                </div>

                {isLoading&& <div className={styles.loading}>
                    <BeatLoader size={30} color="grey" loading={isLoading} />
                </div>}
                <div className={styles.buildingType}>
                    {displayFiveButton && displayFiveButton.map((item) => {
                        return <div key={item.PropertyName} className={styles.buildingButton} onClick={() => selectedPropertyItem(item.PropertyName)}>
                            <p>{item.PropertyName}</p>
                        </div>
                    })}
                </div>

            </div>
            <div className={styles.buttonRow}>
                <div className={styles.signOutButton}>
                    <button onClick={logout}>SIGN OUT</button>
                </div>
                <div className={styles.directionButton}>
                    <button disabled={disableBackBtn} className={disableBackBtn ? styles.disabled : null} onClick={() => backFiveBtn(displayFiveButtonIndex)}>←</button>
                    <button disabled={disableNextBtn} className={disableNextBtn ? styles.disabled : null} onClick={() => nextFiveBtn(displayFiveButtonIndex)}>→</button>
                </div>
            </div>
        </div>
    )
}
