import styles from './css/MenuRow.module.css';

export const MenuRow = ({
    setSelectedPage, selectedPage
}) => {
    return <div className={styles.menuRow}>
        <p>SEATTLE BUILDING DATA VISUALIZATION</p>
        <button className={selectedPage === 1 ? styles.greenButton : null} onClick={() => setSelectedPage(1)}>overview</button>
        <button className={selectedPage === 2 ? styles.greenButton : null} onClick={() => setSelectedPage(2)}>charts</button>
    </div>  
}